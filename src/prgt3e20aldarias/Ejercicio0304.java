/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt3e20aldarias;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Fichero: Ejercicio0304.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */

public class Ejercicio0304 {
  
  public static void main(String[] args ) {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    int numero;
    
    try {
      System.out.print("Introduce numero: ");
      numero = (int)buffer.read()-'0';
      System.out.println("Numero leido: "+numero);
    }
    catch ( Exception e) {
      e.printStackTrace();
    }
  }

}
/* EJECUCION:
Introduce numero: 2
Numero leido: 2
*/